// 1.Transforme la hora actual en segundos.
function calculo(){
    var hora = new Date();
    document.getElementById("segundos").value = (hora.getHours() * 3600) + (hora.getMinutes() * 60) + hora.getSeconds();
    
}

//2.Calcular el área de un triángulo, que es igual a: (base * altura)/2.
function triangulo(){

    var base = document.getElementById("base").value
    var altura = document.getElementById("altura").value

    if(base.lenght == "" || altura.lenght == ""){
        error();
    }
    else
        document.getElementById("area").value = (base * altura)/2;       
}

//3.Calcule la raíz cuadrada de un número impar y muestre el resultado con 3 dígitos.
function calraiz(){

    var numero = document.getElementById("numraiz").value

    if(numero.lenght == ""){
        error();
    }
    else
        var resultado = Math.sqrt(numero);
        document.getElementById("raiz").value = resultado.toFixed(2);
  
}

//4.Ingresar una cadena de texto y mostrar la longitud de la cadena.
function textocadena(){

    var letra = document.getElementById("palabra").value

    document.getElementById("numtexto").value = letra.length;
    
    
}

//5.Concatenar los arrays:  array1(Lunes, Martes, Miércoles, Jueves, Viernes) y array 2 (Sábado, Domingo)
function diasemana(){

    var array1 = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes"];
    var array2 = ["Sabado", "Domingo"];

    var array3 = array1.concat(array2);
    
    document.getElementById("arreglo1").value = array1;
    document.getElementById("arreglo2").value = array2;
    document.getElementById("resultado_arreglo").value = array3;
}

//6.Mostrar la versión del navegador.
function versionapp(){

    document.getElementById("version_nav").value = navigator.appVersion;
}


//7.Mostrar el ancho y la altura de la pantalla.
function tamanopagina(){

    var altura = window.innerHeight;
    var ancho = window.innerWidth;

    console.log(altura);
    console.log(ancho);
    document.getElementById("altura-pagina").value = altura;
    document.getElementById("ancho").value = ancho;
}


//8.Imprimir la página.
function imprimirpagina(){
    window.print();
}


function error() {
    Swal.fire({
        title: 'Error',
        text: 'Campos vacios',
        icon: 'error',
        showConfirmButton: false,
        timer: 500
    })
}